/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api;

import net.pinnacle21.validator.api.events.DiagnosticListener;
import net.pinnacle21.validator.api.events.RuleListener;
import net.pinnacle21.validator.api.events.ValidationEvent;
import net.pinnacle21.validator.api.model.*;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

public interface Validation {
    /**
     * Executes this validation synchronously without the ability to cancel, returning the result to the caller.
     * Same as invoking <code>{@link #execute(CancellationToken)}</code> with a <code>null</code> token
     *
     * @return the generated <code>{@link ValidationResult}</code>
     */
    default ValidationResult execute() {
        return this.execute(null);
    }

    /**
     * Executes this validation synchronously, returning the result to the caller
     *
     * @param cancellationToken a <code>{@link CancellationToken}</code> that can be used to abort the validation
     * @return the generated <code>{@link ValidationResult}</code>
     */
    default ValidationResult execute(CancellationToken cancellationToken) {
        AsyncValidationResult result = new AsyncValidationResult();

        this.executeAsync(cancellationToken)
            .whenComplete((r, ex) -> {
                result.accept(r);
            });

        try {
            return result.await();
        } catch (InterruptedException ex) {
            // TODO: Return a stub ValidationResult instead
            return null;
        }
    }

    /**
     * Executes this validation asynchronously without the ability to cancel
     */
    default CompletionStage<ValidationResult> executeAsync() {
        return this.executeAsync(null);
    }

    /**
     * Executes this validation asynchronously, invoking the provided callback with the result
     *
     * @param cancellationToken  a <code>{@link CancellationToken}</code> that can be used to abort the validation
     */
    CompletionStage<ValidationResult> executeAsync(CancellationToken cancellationToken);

    /**
     *
     * @param listener  the listener to register
     * @return  a new instance with the given listener registered
     */
    Validation withRuleCreationListener(RuleListener listener);

    /**
     *
     * @param listener  the listener to deregister
     * @return  a new instance without the given listener registered
     */
    Validation withoutRuleCreationListener(RuleListener listener);

    /**
     * Adds a <code>{@link Consumer}</code> that will receive <code>{@link Diagnostic}</code> objects as they are
     * created during validation
     *
     * @param listener  the listener to register
     * @return  a new instance with the given listener registered
     */
    Validation withDiagnosticListener(DiagnosticListener listener);

    /**
     * Removes a previously registered <code>{@link Diagnostic}</code> consumer
     *
     * @param listener  the listener to deregister
     * @return  a new instance without the given listener registered
     */
    Validation withoutDiagnosticListener(DiagnosticListener listener);

    /**
     * Adds a <code>{@link Consumer}</code> that will receive <code>{@link ValidationEvent}</code> objects representing
     * the state of the validation at various checkpoints within the process
     *
     * @param listener  the listener to register
     * @return  a new instance with the given listener registered
     */
    Validation withValidationEventListener(Consumer<ValidationEvent> listener);

    /**
     * Removes a previously registered <code>{@link ValidationEvent}</code> consumer
     *
     * @param listener  the listener to deregister
     * @return  a new instance without the given listener registered
     */
    Validation withoutValidationEventListener(Consumer<ValidationEvent> listener);
}