/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api;

import net.pinnacle21.validator.api.events.DiagnosticListener;
import net.pinnacle21.validator.api.events.RuleListener;
import net.pinnacle21.validator.api.events.ValidationEvent;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;

public abstract class BaseValidation<T extends BaseValidation<T>> implements Validation {
    protected final Set<DiagnosticListener> diagnosticListeners = new LinkedHashSet<>();
    protected final Set<Consumer<ValidationEvent>> eventListeners = new LinkedHashSet<>();
    protected final Set<RuleListener> ruleListeners = new LinkedHashSet<>();

    protected abstract T recreate();

    private T recreate(DiagnosticListener diagnosticListener, Consumer<ValidationEvent> eventListener,
            RuleListener ruleListener, boolean isAdding) {
        T target = this.recreate();

        target.diagnosticListeners.addAll(this.diagnosticListeners);
        target.eventListeners.addAll(this.eventListeners);
        target.ruleListeners.addAll(this.ruleListeners);

        addOrRemoveListener(target.diagnosticListeners, diagnosticListener, isAdding);
        addOrRemoveListener(target.eventListeners, eventListener, isAdding);
        addOrRemoveListener(target.ruleListeners, ruleListener, isAdding);

        return target;
    }

    @Override
    public Validation withRuleCreationListener(RuleListener listener) {
        return this.recreate(null, null, listener, true);
    }

    @Override
    public Validation withoutRuleCreationListener(RuleListener listener) {
        return this.recreate(null, null, listener, false);
    }

    @Override
    public Validation withDiagnosticListener(DiagnosticListener listener) {
        return this.recreate(listener, null, null, true);
    }

    @Override
    public Validation withoutDiagnosticListener(DiagnosticListener listener) {
        return this.recreate(listener, null, null, false);
    }

    @Override
    public Validation withValidationEventListener(Consumer<ValidationEvent> listener) {
        return this.recreate(null, listener, null, true);
    }

    @Override
    public Validation withoutValidationEventListener(Consumer<ValidationEvent> listener) {
        return this.recreate(null, listener, null, false);
    }

    private static <T> void addOrRemoveListener(Set<T> listeners, T listener, boolean isAdding) {
        if (listener != null) {
            if (isAdding) {
                listeners.add(listener);
            } else {
                listeners.remove(listener);
            }
        }
    }
}
