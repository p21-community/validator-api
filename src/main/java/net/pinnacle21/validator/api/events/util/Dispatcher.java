/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.events.util;

import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 *
 * @author Tim Stone
 */
public final class Dispatcher {
    private final BiConsumer<String, Throwable> onException;

    public Dispatcher() {
        this(null);
    }

    public Dispatcher(Function<Class, BiConsumer<String, Throwable>> loggerProvider) {
        this.onException = loggerProvider != null
                ? loggerProvider.apply(Dispatcher.class)
                : (s, e) -> {};
    }

    public <T extends List<?>> void dispatchTo(Set<Consumer<T>> consumers, T value) {
        if (value.isEmpty()) {
            return;
        }

        consumers.forEach(unexceptionally(c -> c.accept(value)));
    }

    public <T> void dispatchTo(Set<Consumer<T>> consumers, T value) {
        consumers.forEach(unexceptionally(c -> c.accept(value)));
    }

    public <T, U> void dispatchTo(Set<U> consumers, Function<U, Consumer<T>> map, T value) {
        consumers.forEach(unexceptionally(c -> map.apply(c).accept(value)));
    }

    public <T> void dispatchTo(Set<T> procedures, Function<T, Procedure> map) {
        procedures.forEach(unexceptionally(p -> map.apply(p).execute()));
    }

    public <T> Consumer<T> unexceptionally(Consumer<T> consumer) {
        return c -> {
            try {
                consumer.accept(c);
            } catch (Throwable ex) {
                this.onException.accept("Exception when invoking callback for type " + c.getClass(), ex);
            }
        };
    }
}
