/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.events;

public interface ValidationEvent {
    enum Type {
        Configuring,
        Processing,
        Subprocessing
    }

    enum State {
        Start,
        InProgress,
        Stop
    }

    /**
     * Gets the current progress index. This can represent the current dataset or record
     * being processed, or something like the current record being stored in a cache.
     *
     * @return  the current progress index
     */
    long getCurrent();

    /**
     * Gets the maximum progress index. This is typically the total number of datasets
     * being validated.
     *
     * @return  the maximum progress index
     */
    long getMaximum();

    /**
     * Gets the name of the item currently being processed, if one was provided.
     *
     * @return  the name of the item being processed
     */
    String getName();

    /**
     * Gets the state of the <code>Validator</code> that this event represents.
     *
     * @return  the state represented by this event
     * @see State
     */
    State getState();

    /**
     * Gets the subevent, usually representative of the current record being processed.
     *
     * @return  the subevent, or <code>null</code> if one was not defined
     */
    ValidationEvent getSubevent();

    /**
     * The timestamp that marks the creation time of this event.
     *
     * @return  the time this event occurred
     */
    long getTimestamp();

    /**
     *
     * @return  the type of event
     * @see Type
     */
    Type getType();
}
