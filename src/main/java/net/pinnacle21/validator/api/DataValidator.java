/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api;

import net.pinnacle21.validator.api.model.ConfigOptions;
import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;

public interface DataValidator extends Validator {
    /**
     * Creates a <code>{@link Validation}</code> ready to be executed with the provided source and configuration
     * information
     *
     * @param source  information about the source data to validate
     * @param config  the configuration to use to validate the data
     */
    default Validation prepare(SourceOptions source, ConfigOptions config) {
        return this.prepare(Collections.singletonList(source), config);
    }

    /**
     * Creates a <code>{@link Validation}</code> ready to be executed with the provided source and configuration
     * information
     *
     * @param sources  information about the source data to validate
     * @param config  the configuration to use to validate the data
     */
    default Validation prepare(List<SourceOptions> sources, ConfigOptions config) {
        return this.prepare(sources, config, null);
    }

    /**
     * Creates a <code>{@link Validation}</code> ready to be executed with the provided source and configuration
     * information, using the given validation options
     *
     * @param sources  information about the source data to validate
     * @param config  the configuration to use to validate the data
     * @param options  the validation options to use when validating
     */
    default Validation prepare(List<SourceOptions> sources, ConfigOptions config, ValidationOptions options) {
        return this.prepare(sources, config, options, null);
    }

    /**
     * Creates a <code>{@link Validation}</code> ready to be executed with the provided source and configuration
     * information, using the given validation options. A shared <code>Exectutor</code> can be passed in for complete
     * control over the internal threads used for processing
     *
     * @param sources  information about the source data to validate
     * @param config  the configuration to use to validate the data
     * @param options  the validation options to use when validating
     * @param sharedExecutor  the shared executor, for full control over internal thread management (requires
     *                        feature "SharedThreadpool")
     */
    Validation prepare(List<SourceOptions> sources, ConfigOptions config, ValidationOptions options,
            ExecutorService sharedExecutor);
}
