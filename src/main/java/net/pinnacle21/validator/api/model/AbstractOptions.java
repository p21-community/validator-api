/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

abstract class AbstractOptions {
    protected final Map<String, String> properties = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    AbstractOptions(Map<String, String> properties) {
        this.properties.putAll(properties);
    }

    /**
     * Gets a property value
     *
     * @param property  the name of the property
     * @return  the value of the property if set, else the empty string
     */
    public String getProperty(String property) {
        return this.properties.getOrDefault(property, "");
    }

    /**
     * Gets the set of defined property names
     *
     * @return  the set of defined property names
     */
    public Set<String> getProperties() {
        return Collections.unmodifiableSet(this.properties.keySet());
    }

    /**
     * Indicates whether the given property is set
     *
     * @param property  the name of the property
     * @return  <code>true</code> if the property is set (and not the empty string), <code>false</code> otherwise
     */
    public boolean hasProperty(String property) {
        return !this.properties.getOrDefault(property, "").isEmpty();
    }

    /**
     * Indicates whether the given property is set with the provided value
     *
     * @param property  the name of the property
     * @param value  the value to check for, if the property is set
     * @return  <code>true</code> if the property is set case-insensitively to the given value,
     *          <code>false</code> otherwise
     */
    public boolean hasProperty(String property, String value) {
        return this.properties.getOrDefault(property, "").equalsIgnoreCase(value);
    }

    static abstract class AbstractBuilder<T extends AbstractBuilder<T>> {
        protected final Map<String, String> properties = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        AbstractBuilder() {}

        public T withProperty(String property, String value) {
            this.properties.put(property, value);

            //noinspection unchecked
            return (T)this;
        }
    }
}
