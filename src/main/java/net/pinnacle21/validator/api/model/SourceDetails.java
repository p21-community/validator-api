/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.List;

public interface SourceDetails {
    /**
     * A pre-defined list of properties that a given <code>SourceDetails</code> may provide. Note that the values
     * associated with these properties may change while the underlying source is still being validated.
     *
     * @author Tim Stone
     */
    enum Property implements ConvertibleProperty {
        Class(false),
        Configuration(false),
        Combined(true),
        Corrupted(true),
        DatasetLabel(false),
        FileSize(true),
        Filtered(true),
        Keys(false),
        Label(false),
        Location(false),
        Name(false),
        Records(true),
        Split(true),
        Subname(false),
        Validated(true),
        Variables(true);

        private final boolean isConvertible;

        Property(boolean isConvertible) {
            this.isConvertible = isConvertible;
        }

        /**
         * Indicates whether this property is representable as a boolean or integer
         *
         * @return  <code>true</code> if the property is convertible, <code>false</code> otherwise
         */
        public boolean isConvertible() {
            return this.isConvertible;
        }
    }

    /**
     * Defines the reference types that a source may represent. Source data may have both data and metadata
     * views which are validated separately.
     *
     * @author Tim Stone
     */
    enum Reference {
        Data,
        Metadata
    }

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return  the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    boolean getBoolean(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible
     * @see Property
     * @see #hasProperty(Property)
     */
    Boolean getBoolean(Property property, Boolean defaultValue);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    int getInteger(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible
     * @see Property
     * @see #hasProperty(Property)
     */
    Integer getInteger(Property property, Integer defaultValue);

    /**
     * Gets the parent source of this source, if defined
     *
     * @return  the parent source, if this source represents metadata
     * @see Reference
     */
    SourceDetails getParent();

    /**
     * Gets this <code>SourceDetail</code>'s reference type.
     *
     * @return  the reference type
     * @see Reference
     */
    Reference getReference();

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return  the value of the property as a string
     * @throws IllegalArgumentException  if the property is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    String getString(Property property);

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the value of the property as a string
     * @see Property
     * @see #hasProperty(Property)
     */
    String getString(Property property, String defaultValue);

    /**
     * Checks if this source defines the given variable name
     *
     * @param name  the name of the variable to look for
     * @return  <code>true</code> if the variable is defined, otherwise <code>false</code>
     */
    boolean hasVariable(String name);

    /**
     * Gets the <code>{@link VariableDetails}</code> for the given variable name in this source, if defined
     *
     * @param name  the name of the variable to look for
     * @return  the <code>VariableDetails</code> for the variable with the provided <code>name</code>, otherwise
     *     <code>null</code>
     * @see VariableDetails
     */
    VariableDetails getVariable(String name);

    /**
     * Gets the list of <code>{@link VariableDetails}</code> that describe the variables in this source
     *
     * @return  the list of variables defined for this source
     * @see VariableDetails
     */
    List<VariableDetails> getVariables();

    /**
     * Checks whether or not this <code>EntityDetails</code> defines the given <code>property</code>.
     *
     * @param property  the <code>Property</code> to check the existence of
     * @return  <code>true</code> if the property is defined, <code>false</code> otherwise
     * @see Property
     */
    boolean hasProperty(Property property);

    /**
     * Gets the list of split sources that make up this source, if this source is combined (i.e. from split datasets)
     *
     * @return  the list of subsources joined under the common name represented by this source, or an empty list if this
     *     source is not split
     */
    List<SourceDetails> getSplitSources();
}
