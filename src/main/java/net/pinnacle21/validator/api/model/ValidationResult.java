/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.Collections;
import java.util.Set;

public final class ValidationResult {
    private final boolean completedSuccessfully;
    private final RuleMetrics metrics;
    private final Set<SourceDetails> sources;
    private final Throwable cause;

    private ValidationResult(Builder builder) {
        this.completedSuccessfully = builder.completedSuccessfully;
        this.metrics = builder.metrics;
        this.sources = builder.sources;
        this.cause = builder.cause;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Throwable getCause() {
        return this.cause;
    }

    public boolean completedSuccessfully() {
        return this.completedSuccessfully;
    }

    public RuleMetrics getRuleMetrics() {
        return this.metrics;
    }

    public Set<SourceDetails> getSources() {
        return this.sources;
    }

    public static final class Builder {
        private boolean completedSuccessfully;
        private RuleMetrics metrics;
        private Set<SourceDetails> sources = Collections.emptySet();
        private Throwable cause;

        private Builder() {}

        public synchronized Builder withCompletedSuccessfully(boolean completedSuccessfully) {
            this.completedSuccessfully = completedSuccessfully;
            return this;
        }

        public synchronized Builder withMetrics(RuleMetrics metrics) {
            this.metrics = metrics;
            return this;
        }

        public synchronized Builder withSources(Set<SourceDetails> sources) {
            this.sources = sources;
            return this;
        }

        public synchronized Builder withCause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        public synchronized ValidationResult build() {
            return new ValidationResult(this);
        }
    }
}
