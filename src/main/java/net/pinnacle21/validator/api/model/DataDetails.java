/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

public interface DataDetails {
    /**
     * The type of information a given <code>DataDetails</code> instance represents.
     *
     * @author Tim Stone
     */
    enum Info {
        Data,
        Variable
    }

    /**
     * Gets the numerical position information of this data, for appropriate <code>Info</code> types. This may be the
     * record number where the data can be found, or the column position of the variable in the source's metadata.
     *
     * @return  the numerical position of data where appropriate, otherwise the value <code>-1</code>
     * @see #getInfo()
     */
    int getId();

    /**
     * Gets the <code>Info</code> type associated with this data detail information.
     *
     * @return  the type of location information
     * @see Info
     */
    Info getInfo();

    /**
     * Gets the key that identifies this data, if it's defined
     *
     * @return  the key identifying this data, if defined
     */
    String getKey();

    /**
     * Gets the name information of this data, which is typically a variable name
     *
     * @return  the name that identifies this data
     * @see #getInfo()
     */
    String getName();
}
