/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;

public class SourceOptions extends AbstractOptions {
    public enum StandardTypes implements Type {
        SasTransport(e("xpt")),
        Delimited(e("dlm"),
            e("csv", b -> b.withProperty("Delimiter", ",")),
            e("tab", b -> b.withProperty("Delimiter", "\t")),
            e("pipe", b -> b.withProperty("Delimiter", "|")),
            e("dollar", b -> b.withProperty("Delimiter", "$"))
        ),
        DatasetXml(e("xml"));

        private final Map<String, Consumer<Builder>> extensions = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        StandardTypes(Extension...extensions) {
            for (Extension extension : extensions) {
                this.extensions.put(extension.name, extension.modifier);
            }
        }

        @Override
        public boolean tryModify(String extension, Builder builder) {
            Consumer<Builder> modifier = this.extensions.get(extension);

            if (modifier == null) {
                return false;
            }

            modifier.accept(builder);

            return true;
        }

        private static Extension e(String name) {
            return e(name, b -> {});
        }

        private static Extension e(String name, Consumer<Builder> modifier) {
            return new Extension(name, modifier);
        }

        private static class Extension {
            private final String name;
            private final Consumer<Builder> modifier;

            Extension(String name, Consumer<Builder> modifier) {
                this.name = name;
                this.modifier = modifier;
            }
        }
    }

    public enum StandardStrategies implements NamingStrategy {
        NoSplit((name, ext) -> {
            if (name.equals("DEFINE") && ext.equalsIgnoreCase("xml")) {
                return Optional.empty();
            }

            return Optional.of(builder().withName(name));
        }),
        GenericSplit((name, ext) -> {
            return NoSplit.apply(name, ext).map(builder -> {
                boolean isAdam = name.startsWith("AD") || name.startsWith("AX");
                int size = name.length();
                int padding = 0;

                if (name.startsWith("AP")) {
                    padding = 2;
                } else if (name.startsWith("SUPP")) {
                    padding = 4;
                }

                if (!isAdam && (size == 3 + padding || size == 4 + padding)) {
                    builder.withName(name.substring(0, 2 + padding));
                }

                return builder;
            });
        });


        private final NamingStrategy strategy;

        StandardStrategies(NamingStrategy strategy) {
            this.strategy = strategy;
        }


        @Override
        public Optional<Builder> apply(String name, String extension) {
            return this.strategy.apply(name, extension);
        }
    }

    private final Charset charset;
    private final String name;
    private final String subname;
    private final Type type;
    private final String source;

    private SourceOptions(Builder builder) {
        super(builder.properties);

        this.charset = builder.charset;
        this.name = builder.name;
        this.subname = builder.subname;
        this.type = builder.type;
        this.source = builder.source;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Gets the character set to use when reading this source, if applicable
     *
     * @return  the <code>{@link Charset}</code> of the source, if applicable
     */
    public Charset getCharset() {
        return this.charset;
    }

    /**
     * Gets the name of this source, if one was set. If one was not set, the
     * <code>Validator</code> will derive one automatically.
     *
     * @return  the source's assigned name, or <code>null</code> if one was not set
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the subname for this source, if one was set.
     *
     * @return  the source's subname, or <code>null</code> if one was not set
     */
    public String getSubname() {
        return this.subname;
    }

    /**
     * Gets the connection string that points to this source's data.
     *
     * @return  the source's connection string
     */
    public String getSource() {
        return this.source;
    }

    /**
     * Gets the format type of this source
     *
     * @return  the source type
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Creates a <code>Builder</code> based on a file's name
     *
     * @param file  the file to create a builder for
     * @param namingStrategy  the naming strategy to use (i.e. for split datasets)
     * @return  the <code>Builder</code>, if the naming strategy opted to create one
     */
    public static Optional<Builder> fromFile(File file, NamingStrategy namingStrategy) {
        return fromFile(file, null, namingStrategy);
    }

    /**
     * Creates a <code>Builder</code> based on a file's name
     *
     * @param file  the file to create a builder for
     * @param type  the file type
     * @param namingStrategy  the naming strategy to use (i.e. for split datasets)
     * @return  the <code>Builder</code>, if the naming strategy opted to create one
     */
    public static Optional<Builder> fromFile(File file, Type type, NamingStrategy namingStrategy) {
        String fileName = file.getName();
        int index = fileName.indexOf('.');

        if (index == -1) {
            return Optional.empty();
        }

        String name = fileName.substring(0, index).toUpperCase();
        String extension = fileName.substring(index + 1).toLowerCase();

        return namingStrategy.apply(name, extension)
            .flatMap(builder -> type == null
                ? withType(extension, builder)
                : Optional.of(builder.withType(type)))
            .map(builder -> builder.withSubname(name))
            .map(builder -> builder.withSource(file.getAbsolutePath()));
    }

    /**
     *
     * @param type
     * @param builder
     * @return
     */
    public static Optional<Builder> withType(String type, Builder builder) {
        Type chosenType = null;

        if (type != null && !type.isEmpty()) {
            type = type.toLowerCase();

            for (Type standardType : StandardTypes.values()) {
                if (standardType.tryModify(type, builder)) {
                    chosenType = standardType;

                    break;
                }
            }
        }

        if (chosenType == null) {
            return Optional.empty();
        }

        return Optional.of(builder.withType(chosenType));
    }

    public static final class Builder extends AbstractBuilder<Builder> {
        private Charset charset;
        private String name;
        private String subname;
        private Type type;
        private String source;

        private Builder() {}

        public Builder withCharset(Charset charset) {
            this.charset = charset;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withSubname(String subname) {
            this.subname = subname;
            return this;
        }

        public Builder withType(Type type) {
            this.type = type;
            return this;
        }

        public Builder withSource(String source) {
            this.source = source;
            return this;
        }

        public SourceOptions build() {
            return new SourceOptions(this);
        }
    }

    public interface Type {
        boolean tryModify(String extension, Builder builder);
    }

    public interface NamingStrategy extends BiFunction<String, String, Optional<Builder>> {}
}
