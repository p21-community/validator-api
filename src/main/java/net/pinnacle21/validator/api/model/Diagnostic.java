/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.Map;
import java.util.Set;

public interface Diagnostic {
    enum Type {
        Reject,
        Error,
        Warning,
        Notice;

        /**
         * Returns the appropriate <code>Type</code> based on the provided
         * <code>string</code> representation. If the argument does not match the name
         * of a known type, the default value of <code>Type.Warning</code> is returned
         * instead.
         *
         * @param string  the case-insensitive name of the type
         * @return the appropriate <code>Type</code> if possible, otherwise the default
         *     <code>Type.Warning</code>
         */
        public static Type fromString(String string) {
            Type type = Type.Warning;

            if("Error".equalsIgnoreCase(string)) {
                type = Type.Error;
            } else if("Reject".equalsIgnoreCase(string)) {
                type = Type.Reject;
            } else if("Information".equalsIgnoreCase(string) || "Notice".equalsIgnoreCase(string)) {
                type = Type.Notice;
            }

            return type;
        }
    }

    /**
     * Gets the ID of the rule that triggered this diagnostic
     *
     * @return  the ID of the rule
     */
    String getId();

    /**
     * Gets the formatted message of the rule that triggered this diagnostic
     *
     * @return  the message of the rule, with substitutions from the configuration process
     */
    String getMessage();

    /**
     * Gets the description of the rule that triggered this diagnostic
     *
     * @return  the description of the rule
     */
    String getDescription();

    /**
     * Gets the category of the rule that triggered this diagnostic
     *
     * @return  the category of the rule
     */
    String getCategory();

    /**
     * Gets the type of the rule, as it applies to the execution context
     *
     * @return  the type of the rule
     */
    Type getType();

    /**
     * Gets the context used to create the rule
     *
     * @return  the context used to create the source instance of the rule
     */
    String getContext();

    /**
     * Gets the creation timestamp (in epoch milliseconds) of this diagnostic
     *
     * @return  the timestamp when this diagnostic was created
     */
    long getTimestamp();

    /**
     * Gets the <code>{@link SourceDetails}</code> describing the source of the data that triggered this diagnostic
     *
     * @return  the source information
     */
    SourceDetails getSourceDetails();

    /**
     * Gets the <code>{@link DataDetails}</code> describing the location of the data that triggered this diagnostic
     * within the source medium
     *
     * @return  the data location information
     * @see #getSourceDetails()
     */
    DataDetails getDataDetails();

    /**
     * Gets list of variables collected from the source data when this diagnostic was triggered. The returned set
     * is guaranteed to be in the order the variables existed in the source, even though there's no interface to
     * represent this condition
     *
     * @return  the ordered list of variables
     * @see #getVariable(String)
     */
    Set<String> getVariables();

    /**
     * Gets the value recorded from the source data for the given <code>variable</code> name when this diagnostic was
     * triggered
     *
     * @param variable  the name of the variable to get the value of
     * @return  the associated value
     * @see #getVariables()
     */
    String getVariable(String variable);

    /**
     * Gets the full mapping of variable to value pairs collected from the source data when this diagnostic was
     * triggered
     *
     * @return  the map of variable/value pairs
     */
    Map<String, String> getVariableValues();

    /**
     * Gets a list of property names included in this diagnostic
     *
     * @return  the set of properties, or an empty set if there are none
     */
    Set<String> getProperties();

    /**
     * Gets the value associated with the provided <code>property</code> name
     *
     * @param property  the name of the property to get the value of
     * @return  the associated value
     */
    String getProperty(String property);

    /**
     * Gets the full mapping of property to value pairs set when this diagnostic was triggered
     *
     * @return  the map of property/value pairs
     */
    Map<String, String> getPropertyValues();
}
