/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Tim Stone
 */
public class ConfigOptions extends AbstractOptions {
    private final List<String> configs;
    private final String define;

    private ConfigOptions(Builder builder) {
        super(builder.properties);

        this.configs = Objects.requireNonNull(builder.configs)
            .stream()
            .filter(Objects::nonNull)
            .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
        this.define = builder.define;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Gets the list of connection strings pointing to the one or more configuration files to use for validation
     *
     * @return the list of configuration connection strings
     */
    public List<String> getConfigs() {
        return this.configs;
    }

    /**
     * Gets the connection string information for associated define.xml metadata.
     *
     * @return the connection string pointing to the define.xml metadata, if provided
     */
    public String getDefine() {
        return this.define;
    }

    public static final class Builder extends AbstractBuilder<Builder> {
        private List<String> configs;
        private String define;

        private Builder() {}

        public Builder withConfigs(String...configs) {
            return this.withConfigs(Arrays.asList(configs));
        }

        public Builder withConfigs(List<String> configs) {
            this.configs = configs;
            return this;
        }

        public Builder withDefine(String define) {
            this.define = define;
            return this;
        }

        public ConfigOptions build() {
            return new ConfigOptions(this);
        }
    }
}
