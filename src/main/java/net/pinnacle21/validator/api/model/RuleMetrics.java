/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author Tim Stone
 */
public interface RuleMetrics {
    Set<String> getDomains();
    Set<String> getDomainRules(String domain);
    Map<String, Set<String>> getDomainContexts(String domain);
    long getExecutionCount();
    long getFailureCount();
    long getInvocationCount();
    long getDomainExecutionCount(String domain);
    long getDomainExecutionCount(String domain, Diagnostic.Type type);
    long getDomainFailureCount(String domain);
    long getDomainFailureCount(String domain, Diagnostic.Type type);
    long getDomainInvocationCount(String domain);
    long getDomainInvocationCount(String domain, Diagnostic.Type type);
    int getRuleExecutionCount(String domain, String id, String context);
    int getRuleFailureCount(String domain, String id, String context);
    int getRuleInvocationCount(String domain, String id, String context);
    long getRuleElapsedCount(String domain, String id, String context);
    SourceDetails.Reference getRuleReference(String domain, String id, String context);
    String getRuleMessage(String domain, String id, String context);
    Diagnostic.Type getRuleType(String domain, String id, String context);
}
