/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api.model;

public interface VariableDetails {
    /**
     * A pre-defined list of properties that a given <code>VariableDetails</code> may provide.
     *
     * @author Tim Stone
     */
    enum Property implements ConvertibleProperty {
        Format(false),
        FullFormat(false),
        Label(false),
        Length(true),
        Name(false),
        Order(true),
        Type(false);

        private final boolean isConvertible;

        Property(boolean isConvertible) {
            this.isConvertible = isConvertible;
        }

        /**
         * Indicates whether this property is representable as a boolean or integer
         *
         * @return <code>true</code> if the property is convertible, <code>false</code> otherwise
         */
        public boolean isConvertible() {
            return this.isConvertible;
        }
    }

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return  the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    boolean getBoolean(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible
     * @see Property
     * @see #hasProperty(Property)
     */
    Boolean getBoolean(Property property, Boolean defaultValue);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return  the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    int getInteger(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible
     * @see Property
     * @see #hasProperty(Property)
     */
    Integer getInteger(Property property, Integer defaultValue);

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the value of the property as a string
     * @throws IllegalArgumentException  if the property is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    String getString(Property property);

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @param defaultValue  the value to return if the provided property isn't set
     * @return  the value of the property as a string
     * @see Property
     * @see #hasProperty(Property)
     */
    String getString(Property property, String defaultValue);

    /**
     * Checks whether or not this <code>VariableDetails</code> defines the given
     * <code>property</code>.
     *
     * @param property  the <code>Property</code> to check the existence of
     * @return  <code>true</code> if the property is defined, <code>false</code> otherwise
     * @see Property
     */
    boolean hasProperty(Property property);
}
