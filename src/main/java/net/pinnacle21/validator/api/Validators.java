/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api;

import org.jboss.modules.Module;
import org.jboss.modules.ModuleLoadException;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public final class Validators {
    private static final String BUNDLE_PREFIX = "pinnacle21.validation";
    private static final String DEFAULT_PATH = "modules";

    private Validators() {}

    public static DataValidator dataValidator(String version) {
        return dataValidator(version, null);
    }

    public static DataValidator dataValidator(String version, String path) {
        return load(version, path, DataValidator.class);
    }

    public static DefineValidator defineValidator(String version) {
        return defineValidator(version, null);
    }

    public static DefineValidator defineValidator(String version, String path) {
        return load(version, path, DefineValidator.class);
    }

    public static Optional<Path> resource(String version, String resource) {
        return resource(version, null, resource);
    }

    public static Optional<Path> resource(String version, String path, String resource) {
        if (path == null || path.isEmpty()) {
            path = DEFAULT_PATH;
        }

        Path target = Paths.get(path, BUNDLE_PREFIX.replace(".", "/"), version, resource);

        return target.toFile().isFile()
                ? Optional.of(target)
                : Optional.empty();
    }

    private static <T> T load(String version, String path, Class<T> serviceClass) {
        if (path == null || path.isEmpty()) {
            path = DEFAULT_PATH;
        }

        System.setProperty("module.path", new File(path).getAbsolutePath());

        Throwable cause = null;
        String moduleName = BUNDLE_PREFIX + ":" + version;

        try {
            for (T service : Module.getBootModuleLoader()
                    .loadModule(moduleName)
                    .loadService(serviceClass)
            ) {
                return service;
            }
        } catch (ModuleLoadException ex) {
            cause = ex;
        }

        throw new IllegalArgumentException("Unable to load validation service from module " + moduleName, cause);
    }
}
