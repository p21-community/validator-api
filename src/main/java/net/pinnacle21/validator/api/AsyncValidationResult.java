/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.api;

import net.pinnacle21.validator.api.model.ValidationResult;

import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

class AsyncValidationResult implements Consumer<ValidationResult> {
    private final CountDownLatch latch = new CountDownLatch(1);
    private ValidationResult result;

    @Override
    public void accept(ValidationResult validationResult) {
        this.result = validationResult;
        this.latch.countDown();
    }

    ValidationResult await() throws InterruptedException {
        this.latch.await();

        return this.result;
    }
}
